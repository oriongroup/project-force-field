<?php

class OG_Force_Field_Rewrite_Manager_Test extends \PHPUnit_Framework_TestCase {

	function test_shields_up() {

		$filename  = 'test.txt';
		$new_login = 'protection.php';

		$file_contents = array(
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$wp_login_file = preg_quote( 'wp-login.php' );

		$expected_put_content = array(
			'',
			'# BEGIN ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>',
			'# END ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_file_contents' )
						  ->with( $filename )
						  ->will( $this->returnValue( $file_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_file_contents' )
						  ->with( $filename, $expected_put_content )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->shields_up();

		$this->assertTrue( $results );
	}

	function test_shields_up_with_optional_blocks() {

		$filename         = 'test.txt';
		$new_login        = '54321';
		$optional_block_1 = 'safe-entrance.php';
		$optional_block_2 = '12345';

		$file_contents = array(
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$wp_login_file = preg_quote( 'wp-login.php' );

		$expected_put_content = array(
			'',
			'# BEGIN ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteRule ^' . preg_quote( $optional_block_1 ) . '$ - [F]',
			'RewriteRule ^' . preg_quote( $optional_block_2 ) . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>',
			'# END ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_file_contents' )
						  ->with( $filename )
						  ->will( $this->returnValue( $file_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_file_contents' )
						  ->with( $filename, $expected_put_content )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->shields_up( array( $optional_block_1, $optional_block_2 ) );

		$this->assertTrue( $results );
	}

	function test_shields_up_without_permalinks() {

		$filename  = 'test.txt';
		$new_login = 'protection.php';

		$file_contents = array(
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$wp_login_file = preg_quote( 'wp-login.php' );

		$expected_put_content = array(
			'',
			'# BEGIN ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'</IfModule>',
			'# END ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_file_contents' )
						  ->with( $filename )
						  ->will( $this->returnValue( $file_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_file_contents' )
						  ->with( $filename, $expected_put_content )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login, false );

		$results = $rewrite_manager->shields_up();

		$this->assertTrue( $results );
	}

	function test_reset_shields_with_old_rules() {

		$filename      = 'test.txt';
		$new_login     = 'safe-entrance.php';
		$wp_login_file = preg_quote( 'wp-login.php' );

		$file_contents = array(
			'',
			'# BEGIN ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'</IfModule>',
			'# END ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$expected_put_content = array(
			'',
			'# BEGIN ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>',
			'# END ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_file_contents' )
						  ->with( $filename )
						  ->will( $this->returnValue( $file_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_file_contents' )
						  ->with( $filename, $expected_put_content )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->reset_shields();

		$this->assertTrue( $results );
	}

	function test_shields_up_stops_if_sections_exist() {

		$filename = 'test.txt';
		$new_login = 'protection.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$file_contents = array(
			'',
			'# BEGIN ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>',
			'# END ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_file_contents' )
						  ->with( $filename )
						  ->will( $this->returnValue( $file_contents ) );

		$mock_file_manager->expects( $this->exactly( 0 ) )
						  ->method( 'put_file_contents' );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->shields_up();

		$this->assertTrue( $results );
	}

	function test_shields_down() {

		$filename  = 'test.txt';
		$new_login = 'protection.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$file_contents = array(
			'',
			'# BEGIN ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>',
			'# END ' . OG_Force_Field_Rewrite_Manager::MARKER,
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$expected_put_contents = array(
			'',
			'#line 1',
			'#line 2',
			'#line 3'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_file_contents' )
						  ->with( $filename )
						  ->will( $this->returnValue( $file_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_file_contents' )
						  ->with( $filename, $expected_put_contents )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->shields_down();

		$this->assertTrue( $results );
	}

	function test_check_polarity_when_login_is_old() {

		$filename  = 'test.txt';
		$old_login = 'protection.php';
		$new_login = 'sneaky.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$marker_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $old_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$expected_put_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_contents_from_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER )
						  ->will( $this->returnValue( $marker_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_contents_array_with_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER, $expected_put_contents )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->check_polarity( $new_login );

		$this->assertTrue( $results );
	}

	function test_check_polarity_with_optional_blocks() {

		$filename  = 'test.txt';
		$old_login = 'protection.php';
		$new_login = 'sneaky.php';

		$optional_blocks = array( $old_login );

		$wp_login_file = preg_quote( 'wp-login.php' );

		$marker_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $old_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$expected_put_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteRule ^' . preg_quote( $optional_blocks[0] ) . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_contents_from_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER )
						  ->will( $this->returnValue( $marker_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_contents_array_with_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER, $expected_put_contents )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->check_polarity( $new_login, $optional_blocks );

		$this->assertTrue( $results );
	}

	function test_check_polarity_when_login_is_up_to_date() {

		$filename  = 'test.txt';
		$new_login = 'sneaky.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$marker_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_contents_from_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER )
						  ->will( $this->returnValue( $marker_contents ) );

		$mock_file_manager->expects( $this->exactly( 0 ) )
						  ->method( 'put_contents_array_with_marker' );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->check_polarity( $new_login );

		$this->assertTrue( $results );
	}

	function test_check_polarity_when_permalinks_were_enabled() {

		$filename  = 'test.txt';
		$new_login = 'sneaky.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$marker_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$expected_put_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'</IfModule>'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_contents_from_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER )
						  ->will( $this->returnValue( $marker_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_contents_array_with_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER, $expected_put_contents )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login, false );

		$results = $rewrite_manager->check_polarity( $new_login );

		$this->assertTrue( $results );
	}

	function test_check_polarity_when_permalinks_now_enabled() {

		$filename  = 'test.txt';
		$new_login = 'sneaky.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$marker_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'</IfModule>'
		);

		$expected_put_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_contents_from_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER )
						  ->will( $this->returnValue( $marker_contents ) );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'put_contents_array_with_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER, $expected_put_contents )
						  ->will( $this->returnValue( true ) );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->check_polarity( $new_login );

		$this->assertTrue( $results );
	}

	function test_check_polarity_when_permalinks_still_not_enabled() {

		$filename  = 'test.txt';
		$new_login = 'sneaky.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$marker_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'</IfModule>'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_contents_from_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER )
						  ->will( $this->returnValue( $marker_contents ) );

		$mock_file_manager->expects( $this->exactly( 0 ) )
						  ->method( 'put_contents_array_with_marker' );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login, false );

		$results = $rewrite_manager->check_polarity( $new_login );

		$this->assertTrue( $results );
	}

	function test_check_polarity_when_permalinks_still_enabled() {

		$filename  = 'test.txt';
		$new_login = 'sneaky.php';

		$wp_login_file = preg_quote( 'wp-login.php' );

		$marker_contents = array(
			'<IfModule mod_rewrite.c>',
			'RewriteEngine On',
			'RewriteRule ^' . preg_quote( $new_login ) . '$ wp-login.php [NC,L]',
			'RewriteCond %{THE_REQUEST} ' . $wp_login_file . ' [NC]',
			'RewriteRule ^' . $wp_login_file . '$ - [F]',
			'RewriteCond %{REQUEST_URI}  ^/$',
			'RewriteCond %{QUERY_STRING} ^/?author=([0-9]*)',
			'RewriteRule ^(.*)$ - [F]',
			'</IfModule>'
		);

		$mock_file_manager = $this->getMock( 'FZ_WordPress_File_Manager' );

		$mock_file_manager->expects( $this->once() )
						  ->method( 'get_contents_from_marker' )
						  ->with( $filename, OG_Force_Field_Rewrite_Manager::MARKER )
						  ->will( $this->returnValue( $marker_contents ) );

		$mock_file_manager->expects( $this->exactly( 0 ) )
						  ->method( 'put_contents_array_with_marker' );

		$rewrite_manager = new OG_Force_Field_Rewrite_Manager( $mock_file_manager, $filename, $new_login );

		$results = $rewrite_manager->check_polarity( $new_login );

		$this->assertTrue( $results );
	}
}