<?php

class FZ_WordPress_File_Manager_Test extends \PHPUnit_Framework_TestCase {

	function test_file_exists_when_it_does() {
		$filename = '/path/to/test.txt';

		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'exists' )
						   ->with( $filename )
						   ->will( $this->returnValue( true ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$is_writable = $wp_file_manager->file_exists( $filename, $mock_wp_filesystem );

		$this->assertEquals( true, $is_writable );
	}

	function test_is_file_writable_when_it_is() {
		$filename = '/path/to/test.txt';

		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'exists' )
						   ->with( $filename )
						   ->will( $this->returnValue( true ) );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'is_writable' )
						   ->with( $filename )
						   ->will( $this->returnValue( true ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$is_writable = $wp_file_manager->is_file_writeable( $filename, $mock_wp_filesystem );

		$this->assertEquals( true, $is_writable );
	}

	function test_is_file_writable_when_unexistent_and_dir_writable() {
		$filename = '/path/to/test.txt';
		$dirname  = dirname( $filename );

		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'exists' )
						   ->with( $filename )
						   ->will( $this->returnValue( false ) );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'is_writable' )
						   ->with( $dirname )
						   ->will( $this->returnValue( true ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$is_writable = $wp_file_manager->is_file_writeable( $filename, $mock_wp_filesystem );

		$this->assertEquals( true, $is_writable );
	}

	function test_is_file_writable_when_exists_but_not_writable() {
		$filename = '/path/to/test.txt';
		
		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'exists' )
						   ->with( $filename )
						   ->will( $this->returnValue( true ) );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'is_writable' )
						   ->with( $filename )
						   ->will( $this->returnValue( false ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$is_writable = $wp_file_manager->is_file_writeable( $filename, $mock_wp_filesystem );

		$this->assertEquals( false, $is_writable );
	}

	function test_is_file_writable_when_nonexistant_and_dir_not_writable() {
		$filename = '/path/to/test.txt';
		$dirname  = dirname( $filename );
		
		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'exists' )
						   ->with( $filename )
						   ->will( $this->returnValue( false ) );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'is_writable' )
						   ->with( $dirname )
						   ->will( $this->returnValue( false ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$is_writable = $wp_file_manager->is_file_writeable( $filename, $mock_wp_filesystem );

		$this->assertEquals( false, $is_writable );
	}
    
	function test_get_file_contents_trims_strings() {
		$filename = 'test.txt';
		$file_contents = array(
			"\n",
			"#line 1\n",
			"#line 2\r\n",
			"#line 3\r",
			""
		);

		$expected_contents = array(
			'',
			'#line 1',
			'#line 2',
			'#line 3',
			''
		);

		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'get_contents_array' )
						   ->with( $filename )
						   ->will( $this->returnValue( $file_contents ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$actual_contents = $wp_file_manager->get_file_contents( $filename, $mock_wp_filesystem );

		$this->assertEquals( $expected_contents, $actual_contents );
	}
    
	function test_put_file_contents() {
		$filename = 'test.txt';

		$content_to_put = array(
			'',
			'#line 1',
			'#line 2',
			'#line 3',
			''
		);

		$expected_contents_param = implode(
			"\n",
			array(
				'',
				'#line 1',
				'#line 2',
				'#line 3',
				''
			)
		);

		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'put_contents' )
						   ->with( $filename, $expected_contents_param )
						   ->will( $this->returnValue( true ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$results = $wp_file_manager->put_file_contents( $filename, $content_to_put, $mock_wp_filesystem );

		$this->assertTrue( $results );
	}

	function test_prepend_file_with_content() {
		$filename = 'test.txt';
		$file_contents = array(
			"\n",
			"#line 1\n",
			"#line 2\r\n",
			"#line 3\r",
			""
		);

		$content_to_prepend = array(
			'',
			'#line 4',
			'#line 5',
			'#line 6',
			''
		);

		$expected_contents_param = implode(
			"\n",
			array(
				'',
				'#line 4',
				'#line 5',
				'#line 6',
				'',
				'',
				'#line 1',
				'#line 2',
				'#line 3',
				''
			)
		);

		$mock_wp_filesystem = $this->getMock( 'Testing_WP_Filesystem_Base' );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'get_contents_array' )
						   ->with( $filename )
						   ->will( $this->returnValue( $file_contents ) );

		$mock_wp_filesystem->expects( $this->once() )
						   ->method( 'put_contents' )
						   ->with( $filename, $expected_contents_param )
						   ->will( $this->returnValue( true ) );

		$wp_file_manager = new FZ_WordPress_File_Manager();

		$results = $wp_file_manager->prepend_file_with_content( $filename, $content_to_prepend, $mock_wp_filesystem );

		$this->assertTrue( $results );
	}

}

class Testing_WP_Filesystem_Base {

	function get_contents_array($file) {
		return false;
	}

	function put_contents( $file, $contents, $mode = false ) {
		return false;
	}

	function exists($file) {
		return false;
	}

	function is_writable($file) {
		return false;
	}

}