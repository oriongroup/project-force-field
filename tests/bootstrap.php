<?php

error_reporting(E_ALL | E_STRICT);
ini_set("display_errors", 1);

$classes_dir = dirname( __DIR__ ) . '/classes/';

$class_files = array();
$class_files[] = 'class-base-file-manager.php';
$class_files[] = 'class-wordpress-file-manager.php';
$class_files[] = 'class-force-field-rewrite-manager.php';
$class_files[] = 'class-base-system-manager.php';
$class_files[] = 'class-wordpress-system-manager.php';
$class_files[] = 'class-force-field.php';

foreach( $class_files as $file ) {
	require_once( $classes_dir . $file );
}